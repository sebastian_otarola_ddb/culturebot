const bodyparser = require('body-parser'),
      express    = require('express'),
      app        = express(),
      cors       = require('cors')

const host      = process.env.HOST || '0.0.0.0',
      port      = process.env.PORT || 3000,
      routes    = require('./routes');


//move slacktoken out of root
const slackToken = "xoxp-2155818235-3976711347-263969960002-0386a7426fe349254d454b05490cf29c"

app.set('port', port)
app.use(cors() )
app.use( bodyparser.urlencoded({ extended: true }) )
app.use( bodyparser.json())
app.use('/', routes)
app.use(express.static(__dirname + '/public'))


  // send to specific slackchannel ID channel -  for storage purpose or follow a flow.
  // fetch('https://slack.com/api/chat.postMessage', {
  //   method: 'POST',
  //   headers: {
  //     Authorization: `Bearer ${Token}`,
  //     'Content-Type': 'application/json'
  //   },
  //   body: JSON.stringify({channel: 'G0D04414Y', text:'sending to channel'})
  // })
  // .then(data => {return data.text()})
  // .then(data => console.log(data))
  // .catch(err => console.log(err))


/* }) */

app.listen(port, host, () => {
  console.log('example app running at port 3000')
})
