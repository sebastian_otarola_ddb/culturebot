const Router = require('express').Router()

Router.get('/status', (req, res) => {
  res.json({
    status: 200, 
    message: 'System online'
  })
})

module.exports = Router