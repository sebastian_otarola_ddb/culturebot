const Router = require('express').Router()
const MetaInspector = require('node-metainspector')

Router.post('/getMeta', (req, res) => {
  console.log(req.body);
  console.log(req.params);
  /* res.send('finish') */
  const urlToInspect = req.body.urlToInspect
  return new Promise((resolve, reject) => {
    if(urlToInspect) {
      const metaClient = new MetaInspector(urlToInspect, { timeout: 5000 })
      metaClient.on('fetch', () => {
        const post = {
          title: metaClient.ogTitle ? metaClient.ogTitle : metaClient.title,
          content: metaClient.ogDescription ? metaClient.ogDescription : metaClient.description,
          imageUrl: metaClient.image ? metaClient.image : 'https://culturealerts.com/wp-content/uploads/2018/09/brainradar.png',
          remoteUrl: metaClient.url
        }
        //returns the post object
        res.send(post)
        resolve(post)
      })
      //rejects the post object on meta client error.
      metaClient.on('error', (err) => {
        res.status(500).send('Invalid link.')
      })
      //Executes the MetaClient to fetch the specified URL.
      metaClient.fetch()

    } else {
      res.status(500).send('no link was passed.')
      reject( new Error('rejected') )
    }
    
  })

})

module.exports = Router