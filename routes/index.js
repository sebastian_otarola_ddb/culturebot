const Router = require('express').Router()
const news = require('./news')
const getUrlMeta = require('./getUrlMeta')
const path = require('path')
const botStatus = require('./Botstatus')
const nordSlack = require('./nordSlack')

Router.use(news)
Router.use(getUrlMeta)
Router.use(botStatus)
Router.use(nordSlack)

Router.get('/', (req, res) => {
	res.sendFile( path.resolve('public/index.html'))
})

module.exports = Router