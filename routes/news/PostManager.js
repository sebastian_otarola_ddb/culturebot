const wordpress = require('wordpress')
const MetaInspector = require('node-metainspector')
const wpConfig = require('../../settings/botConfig')

const PostManager = {
  createPost(url, site_url, categories) {
    const urlToInspect = url
    return new Promise((resolve, reject) => {

      const metaClient = new MetaInspector(urlToInspect, { timeout: 5000 })
      metaClient.on('fetch', () => {
        const post = {
          title: metaClient.ogTitle ? metaClient.ogTitle : metaClient.title,
          content: metaClient.ogDescription ? metaClient.ogDescription : metaClient.description,
          imageUrl: metaClient.image ? metaClient.image : 'https://culturealerts.com/wp-content/uploads/2018/09/brainradar.png',
          remoteUrl: metaClient.url
        }
        resolve(createWordPressPost(post, site_url, categories))
      })
      metaClient.on('error', (err) => {
        reject(`Cant find meta info: ${err}`)
      })
      metaClient.fetch()
    })
  }
}

const createWordPressPost = (post, site_url, categories) => {
  return new Promise((resolve, reject) => {
    const wpClient = new wordpress.createClient({
      // url: `http://culture/${site_url}`,
      url: site_url,
      username: 'admin',
      password: '&h#4T5qGf3%oejI1v3'
    })
    wpClient.newPost({
      title: post.title,
      content: post.content,
      status: post.status || 'publish',
      termNames: {
        "category": categories,
      },
      customFields: [
        {
          key: 'remote_image_location',
          value: post.imageUrl
        },
        {
          key: 'remote_link',
          value: post.remoteUrl
        },
        {
          key: 'posted_by',
          value: post.addedByUser
        }
      ]
    }, (err, data) => {
      if (err) { reject(`something went wrong: ${err}`) }
      else { resolve('post created successfully') }
    })
  })
}

module.exports = PostManager