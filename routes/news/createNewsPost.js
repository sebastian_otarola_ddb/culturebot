const Router = require('express').Router()
const urlMatcher = require('../../utils/urlMatcher')
const PostManager = require('./PostManager.js')
const fetch = require('node-fetch')
const config = require('../../settings/botConfig')
const MailManager = require('../../utils/mail-manager')


Router.post('/news', (req, res) => {
	const parsedUrlFromText = req.body.text.replace(/^[, ]+|[, ]+$|[, ]+/g, " ").trim().split(' ')
	const theUrl = parsedUrlFromText[0]
	const SLACK_TEAM_DOMAIN = req.body.team_domain
	const CATEGORIES = req.body.categories
	const SITE_URL = req.body.site_url
	console.log(theUrl, SITE_URL, CATEGORIES)
	createThePost( theUrl, SITE_URL, CATEGORIES )

// Check if account is active in wordpress backend. This points to Multi ADMIN sites registered users.

	// fetch( config.wordpress.acf_url )
	// .then( data => data.json())
	// .then( response => {
	// 	const PAGE = response[0]
	// 	let ACCOUNT_FOUND = false // sets this to true if the active account is found. Else respond with error code.
	// 	const ACCOUNTS = PAGE.acf.slack_teams

	// 	ACCOUNTS.forEach( account => {
	// 		const ACCOUNT_COUNTRY_URL = account.country_site
	// 		const ACCOUNT_SLACK_TEAM = account.country_slack_team
	// 		const ACTIVE_ACCOUNT = account.is_active
			
	// 		if(ACCOUNT_SLACK_TEAM == SLACK_TEAM_DOMAIN && ACTIVE_ACCOUNT) {
	// 			ACCOUNT_FOUND = true
	// 			createThePost( theUrl, ACCOUNT_COUNTRY_URL, CATEGORIES )
	// 		}
	// 	})

	// 	if( !ACCOUNT_FOUND ) {
	// 		res.status(400).send({
	// 			"attachments": [{
	// 				"pretext": "Something is wrong 👎",
	// 				"title": "Your account is inactive at the moment. Contact your admin.",
	// 				"color": "#FD6B9E"
	// 			}]
	// 		})
	// 	}
	// })
	// .catch( err => console.log('err ', err))

	// Only run this function when user account is cleared.
	//send it to wordpress process in PostManager.
	function createThePost(theUrl, site_url, categories) {
		
		if( urlMatcher(theUrl) ) {
			console.log('here inside')
			PostManager.createPost(theUrl, site_url, categories)
			.then( data => {
				const icons_array = [
					"🚀", "🤘", "😜", "👌🏽",
					"🤙", "🖖", "🦄", "🤖",
					"❤️", "🤝", "🌮", "🍕",
					"🌭", "🍔" ]
				const randomIcon = icons_array[Math.floor(Math.random()*icons_array.length)];
				console.log('should send create ding.')
				/* res.send(`Post created! ${randomIcon}`) */
				res.json({ response: `Post created! ${randomIcon}` })
			})
			.catch( err => {
				console.log('something went wrong when creating a post.')
				// MailManager.post(
				// 	`Could not create a post in wordpress ${err}.
				// 	\n URL: ${theUrl}
				// 	\n From: ${ACCOUNT.country_slack_team}`);	
				/* res.send(`error: ${err}`) */
				res.status(400).send({ "response": "Could not create post" })
			})

		} else {
			// If Link fails validation, send this error message.
			MailManager.post('Someone pasted an invalid URL');
			res.send('Invalid URL. Check the URL for typos.')
		}

	}
	
})

module.exports = Router 