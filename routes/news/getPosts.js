const Router = require('express').Router()
const wordpress = require('wordpress')
const wpConfig = require('../../settings/botConfig')

const wpClient = new wordpress.createClient({
  url: wpConfig.wordpress.url +'/sweden',
  username: 'admin',
  password: 'admin'
})

Router.get('/getPosts', (req, res) => {

  wpClient.getPosts( (err, posts) => {
    if(err) {
      res.send('error on thingS: ', err)
    } else {
      res.send(posts)
    }

  })
})

module.exports = Router