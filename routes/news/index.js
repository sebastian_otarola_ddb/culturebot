const Router = require('express').Router()
const createNewsPost = require('./createNewsPost')
const getPosts = require('./getPosts')

Router.use(createNewsPost)
Router.use(getPosts)

module.exports = Router