const Router = require('express').Router()
const urlMatcher = require('../utils/urlMatcher')
const PostManager = require('./news/PostManager')

Router.post('/nordSlack', (req, res) => {
  const parsedUrlFromText = req.body.text.replace(/^[, ]+|[, ]+$|[, ]+/g, " ").trim().split(' ')
	let parsedURL = parsedUrlFromText[1]
  const SLACK_TEAM_DOMAIN = req.body.team_domain
  const SLACK_APP_TOKEN = 'ne0DxTn8C0W3G57prpTuqfqR'

  const regExPattern = /(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})[^>\s]/g
  const BLOG_URL = 'http://nord.culturealerts.com'
  

  //Cleaned up the URL and removed any abnormalities
  if( req.body.token == SLACK_APP_TOKEN) {
    if(parsedURL.match(regExPattern)) {
      parsedURL = parsedURL.match(regExPattern)[0]
      createPost( parsedURL )
    } else {
      res.status(200).send({
           "attachments": [{
             "pretext": "Något gick snett där. 👎",
             "title": "Kolla om du har skrivt rätt länk.",
             "color": "#FD6B9E"
           }]
      })
    }
  } else {
    res.status(200).send({
      "attachments": [{
        "pretext": "UNAUTHORIZED. 👎",
        "title": "You don't have permissions to post here.",
        "color": "#FD6B9E"
      }]
 })
  }
  

  function createPost( url ) {
    if( urlMatcher( url ) ) {
      PostManager.createPost( url, BLOG_URL)
      .then( data => {
        const icons_array = [
					"🚀", "🤘", "😜", "👌🏽",
					"🤙", "🖖", "🦄", "🤖",
					"❤️", "🤝", "🌮", "🍕",
					"🌭", "🍔" ]
				const randomIcon = icons_array[Math.floor(Math.random()*icons_array.length)];
				console.log('should send create ding.')
				/* res.send(`Post created! ${randomIcon}`) */
				res.json({ text: `Posten ligger på skärmen nu! ${randomIcon}` })
      })
      .catch( err => {
        res.status(400).send({ "response": "Could not create post."})
      })
    } else {
      res.status(400).json({ text: 'Sorry. Invalid Link'})
    }
  }

})

module.exports = Router